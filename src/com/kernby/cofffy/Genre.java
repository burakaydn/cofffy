package com.kernby.cofffy;

import java.util.Calendar;
import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class Genre {
	public HashMap<String,Integer> coffeeType;
	private Context context;
	private String[] americanoArray = {"indie%20pop","optimistic","experimental","country","acoustic","alternative","soul","freak%20folk","america"};
	private String[] espressoArray = {"hangover","folk","icelandic","lounge","down%20tempo","folk%20rock","classic%20rock","alternative%20rock","indie%20rock"};
	private String[] cappuccinoArray = {"new%20wave","post-punk","electro%20pop","indie%20hip%20hop","lo-fi","neo%20soul","rnb","chillout","trip%20hop","anti%20folk"};
	private String[] latteArray = {"Minimal","energetic","dreamy","rhythmic","piano","jazz","saddle%20creek","female%20vocalists","dance"};
	private String[] filterCoffeeArray = {"smooth","relax","twee","chamber%20pop","musical","soundtrack","melodramatic"};
	private String[] macchiatoArray = {"dance","minimal%20techno","progressive","tech%20house","electronica","punk","concrete","pop%20punk","swag"};
	private String[] mochaArray = {"creativity","gypsy","jazz","swing","electronic","electronica","acoustic","psychedelic","dupstep"};
	
	public Genre(Context context){
		this.context = context;
	}
	
	public void initializeGenres(){
		generateSharedPreferences("americano", americanoArray);
		generateSharedPreferences("espresso", espressoArray);
		generateSharedPreferences("cappuccino", cappuccinoArray);
		generateSharedPreferences("latte", latteArray);
		generateSharedPreferences("filterCoffee", filterCoffeeArray);
		generateSharedPreferences("macchiato", macchiatoArray);
		generateSharedPreferences("mocha", mochaArray);
	}
	
	public String getGenre(String coffeeType){
		Integer genrePageNumber = 0;
		String genre = null;
		//TODO add some genre which coffee type has less genre than 9;
		Integer genreIndex = Math.min(Calendar.getInstance().get(Calendar.SECOND)%10, 5);
		SharedPreferences sp = context.getSharedPreferences(coffeeType, 0);
		
		if (coffeeType.equals("americano")) {
			genre = americanoArray[genreIndex];
		}else if (coffeeType.equals("espresso")) {
			genre = espressoArray[genreIndex];
		}else if (coffeeType.equals("cappuccino")) {
			genre = cappuccinoArray[genreIndex];
		}else if (coffeeType.equals("latte")) {
			genre = latteArray[genreIndex];
		}else if (coffeeType.equals("filterCoffee")) {
			genre = filterCoffeeArray[genreIndex];
		}else if (coffeeType.equals("macchiato")) {
			genre = macchiatoArray[genreIndex];
		}else if (coffeeType.equals("mocha")) {
			genre = mochaArray[genreIndex];
		}
		
		genrePageNumber = sp.getInt(genre, 0);
		return genre + "," + Integer.toString(genrePageNumber);
	}
	
	public void updateGenrePageNumber(String coffeetype, String genre, Integer genrePageNumber){
		SharedPreferences sp = context.getSharedPreferences(coffeetype, 0);
		SharedPreferences.Editor editor = sp.edit();
		editor.putInt(genre, genrePageNumber);
		editor.commit();
	}
	
	private void generateSharedPreferences(String coffeeType, String[] genreArray){
		SharedPreferences sp = context.getSharedPreferences(coffeeType, 0);		
		SharedPreferences.Editor editor = sp.edit();
		for (String genre : genreArray) {
			editor.putInt(genre, 0);
		}
		editor.commit();
	}
}
