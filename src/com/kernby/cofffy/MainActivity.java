package com.kernby.cofffy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getName();
	private boolean isInternetAvailable = false;
	RelativeLayout rlButtonWrapper;
	Button btnAmericano;
	Button btnCappuccino;
	Button btnEspresso;
	Button btnFilterCoffee;
	Button btnLatte;
	Button btnMocha;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btnAmericano = (Button) findViewById(R.id.btnAmericano);
		btnCappuccino = (Button) findViewById(R.id.btnCappuccino);
		btnEspresso = (Button) findViewById(R.id.btnEspresso);
		btnFilterCoffee = (Button) findViewById(R.id.btnFilterCoffee);
		btnLatte = (Button) findViewById(R.id.btnLatte);
		btnMocha = (Button) findViewById(R.id.btnMocha);
		rlButtonWrapper = (RelativeLayout) findViewById(R.id.rlButtonWrapper);
		rlButtonWrapper.setBackgroundResource(Helper.getRandomBackground());

		isInternetAvailable = Helper.isInternetAvailable(getApplicationContext());
		if (!isInternetAvailable) {
			//TODO: Anlamlı uygulamayı terketmeye sebep olmayacak bağlantı yok uyarısı, eğer net idle ise ??
			Toast.makeText(getApplicationContext(), "Missing Internet Connection!!", Toast.LENGTH_SHORT).show();
		}
		if (isFirstRun()) {
			Genre genre = new Genre(getApplicationContext());
			genre.initializeGenres();
		}
	}
		
	public void buttonClick(View v){
		isInternetAvailable = Helper.isInternetAvailable(getApplicationContext());
		if (isInternetAvailable) {
			String coffeeType = v.getTag().toString();
			Intent playerIntent = getPlayerActivityIntent(v.getTag().toString());
			//TODO bu aktivite başlamadan kullanıcı butona bir çok kez basarsa ?
			// kahve türü hem statik hem intent ile gönderiliyor
			if (Helper.coffeeType != null && coffeeType.equals(Helper.coffeeType)) {
				playerIntent.setAction(PlayerActivity.ACTION_CONTINUE);
				startActivity(playerIntent);
			}else {
				Helper.trackList.clear();
				Helper.activeTrackListIndex = 0;
				Helper.coffeeType = coffeeType;
				playerIntent.setAction(PlayerActivity.ACTION_START);
				startActivity(playerIntent);
			}
		}else {
			Toast.makeText(getApplicationContext(), "Missing Internet Connection!!", Toast.LENGTH_SHORT).show();
		}
		
	}
		
	private Intent getPlayerActivityIntent(String coffeeType){
		Intent playerIntent = new Intent(getApplicationContext(),PlayerActivity.class);
		playerIntent.putExtra(PlayerActivity.COFFEE_TYPE, coffeeType);
		return playerIntent;
	}
	
	private boolean isFirstRun(){
		SharedPreferences sp = getSharedPreferences("settings",0);
		boolean isFirstRun = sp.getBoolean("isFirstRun", true);
		SharedPreferences.Editor editor = sp.edit();
		editor.putBoolean("isFirstRun", false);
		editor.commit();
		return isFirstRun;
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
}
