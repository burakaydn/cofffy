package com.kernby.cofffy;


public class Track {
	private String ArtistDesc;
	private String ArtistName;
	private Integer Duration;
	private String ImageUrl;
	private String Name;
	private String Permalink;
	private String StreamUrl;
	private String ImagePath;
	
	public Track(){
	}
	
	public Track(String artistDesc, String artistName, String imageUrl, String name, String streamUrl){
		this.ArtistDesc = artistDesc;
		this.ArtistName = artistName;
		this.ImageUrl = imageUrl;
		this.Name = name;
		this.StreamUrl = streamUrl;
	}
	
	public String getArtistDesc() {
		return ArtistDesc;
	}
	public void setArtistDesc(String artistDesc) {
		ArtistDesc = artistDesc;
	}
	public String getArtistName() {
		return ArtistName;
	}
	public void setArtistName(String artistName) {
		ArtistName = artistName;
	}
	public Integer getDuration() {
		return Duration;
	}

	public void setDuration(Integer duration) {
		Duration = duration;
	}

	public String getImageUrl() {
		return ImageUrl;
	}
	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	
	public String getPermalink() {
		return Permalink;
	}

	public void setPermalink(String permalink) {
		Permalink = permalink;
	}

	public String getStreamUrl() {
		return StreamUrl;
	}

	public void setStreamUrl(String streamUrl) {
		StreamUrl = streamUrl;
	}

	public String getImagePath() {
		return ImagePath;
	}

	public void setImagePath(String imagePath) {
		ImagePath = imagePath;
	}
}
