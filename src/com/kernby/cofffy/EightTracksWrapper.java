package com.kernby.cofffy;
import java.util.Random;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class EightTracksWrapper implements Wrapper {
	private static final String TAG = EightTracksWrapper.class.getSimpleName();
	private static final String API_KEY = "060784c14729f6209751ee2891c3de497201d456";
	private final String BASE_URL = "http://8tracks.com/mixes.json?api_key="+API_KEY;
	Genre genreGenerator;
	Context context;
	String genre;
	Integer genrePageNumber;
	
	public EightTracksWrapper(Context context){
		this.context = context;
	}
	
	@Override
	public Track findTrack(String coffeeType) {
		Track track = new Track();
//		String url = BASE_URL+"&sort=popular&per_page=10&page=1&tag=" + genres;
		String url = generateUrl(coffeeType);
		Log.i(TAG,"URL: " + url);
		String trackUrl;
		try {
			JSONObject mixesObject = Helper.getJson(url).getJSONObject(0);
//			JSONObject mixesObject = new Helper.getJson().execute(url).get().getJSONObject(0);
			String mixesID = mixesObject.getString("id");
			JSONObject mixObject = mixesObject.getJSONArray("mixes").getJSONObject(0);
			String mixID = mixObject.getString("id");
			genrePageNumber = mixesObject.getInt("total_pages");
			genreGenerator.updateGenrePageNumber(coffeeType, genre, genrePageNumber);
			
			trackUrl = "http://8tracks.com/mix_sets/"+mixesID+"/play.json?api_key="+API_KEY+"&mix_id=" + mixID;
//			JSONObject trackObject = new Helper.getJson().execute(trackUrl).get().getJSONObject(0).getJSONObject("set").getJSONObject("track");
			JSONObject trackObject = Helper.getJson(trackUrl).getJSONObject(0).getJSONObject("set").getJSONObject("track");
			track.setArtistDesc("");
			track.setArtistName(trackObject.getString("performer"));
			track.setDuration(0);
			track.setImageUrl(mixObject.getJSONObject("cover_urls").getString("sq250"));
			track.setName(trackObject.getString("name"));
			track.setPermalink(mixObject.getString("restful_url"));
			track.setStreamUrl(trackObject.getString("url"));
			track.setImagePath(Helper.getImagePath(track.getImageUrl(), "/data/data/com.kernby.cofffy/cache"));
			
		} catch (JSONException e) {
			Log.e(TAG,e.toString());
		}
		
		return track;
	}
	
	@Override
	public String generateUrl(String coffeeType) {
		genreGenerator = new Genre(context);
		String rawGenre = genreGenerator.getGenre(coffeeType);
		genre = rawGenre.split(",")[0];
		genrePageNumber = Integer.valueOf(rawGenre.split(",")[1]);
		if (genrePageNumber > 0) {
			genrePageNumber = new Random().nextInt(genrePageNumber);
		}else if(genrePageNumber == 0){
			genrePageNumber = 1;
		}
		Log.i(TAG,"COFFEE: " + coffeeType + " PAGE: " + genrePageNumber + " TAG: " + genre);
		String url = BASE_URL + "&sort=popular&per_page=1&page="+ genrePageNumber +"&tag=" + genre;
		return url;
	}
	
	
}
