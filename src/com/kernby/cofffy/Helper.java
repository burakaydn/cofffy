package com.kernby.cofffy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class Helper {
	private static final String TAG = Helper.class.getSimpleName();
	private static JSONArray mJSONArray;
	public static ArrayList<Track> trackList = new ArrayList<Track>();
	public static int activeTrackListIndex;
	public static String coffeeType;
	public static boolean isPlaying; 

	public static JSONArray getJson(String Url) {
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(Url);
		HttpResponse response;
		try {
			response = client.execute(httpGet);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				HttpEntity httpEntity = response.getEntity();
				if (httpEntity != null) {
					String json = convertISToString(httpEntity.getContent())
							.trim();
					if (!json.startsWith("[")) {
						json = "[" + json + "]";
					}
					mJSONArray = new JSONArray(json);
				}
			}
		} catch (ClientProtocolException e) {
			Log.e(TAG, e.toString());
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		} catch (IllegalStateException e) {
			Log.e(TAG, e.toString());
		} catch (JSONException e) {
			Log.e(TAG, e.toString());
		}
		return mJSONArray;
	}

	public static int getRandomBackground() {
		int bgs[] = { R.drawable.cofffy_player_01, R.drawable.cofffy_player_02,
				R.drawable.cofffy_player_03, R.drawable.cofffy_player_04,
				R.drawable.cofffy_player_05, R.drawable.cofffy_player_06 };
		Random r = new Random();
		int position = r.nextInt(bgs.length - 1);
		return bgs[position];
	}

	public static String getImagePath(String url, String cacheDir) {
		String fileName = null;
		try {
//			fileName = url.split("/")[7];
			Pattern pattern = Pattern.compile("([|.|\\w|])*\\.(?:jpg|gif|png|jpeg)",Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(url);
			while(matcher.find()){
				fileName = matcher.group();
			}
		} catch (Exception e) {
			fileName = null;
		}
		if (fileName != null) {
			File imageFile = new File(cacheDir, fileName);
			File tempCacheFile = new File(cacheDir);
			FileOutputStream fileOutputStream = null;
			InputStream inputStream = null;
			HttpEntity entity = null;
			if (!tempCacheFile.exists()) {
				tempCacheFile.mkdirs();
				tempCacheFile = null;
			}
			HttpClient client = new DefaultHttpClient();
			final HttpGet httpGet = new HttpGet(url);
			try {
				HttpResponse response = client.execute(httpGet);
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode == HttpStatus.SC_OK) {
					entity = response.getEntity();
					if (entity != null) {
						inputStream = entity.getContent();
						fileOutputStream = new FileOutputStream(imageFile);
						byte[] buffer = new byte[1024];
						int byteRead = 0;
						while ((byteRead = inputStream.read(buffer)) != -1) {
							fileOutputStream.write(buffer, 0, byteRead);
						}
					}
				} else {
					Log.e(TAG, "Error " + statusCode
							+ " while retrieving bitmap from " + url);
					return null;
				}
				if (inputStream != null) {
					inputStream.close();
				}
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
				entity.consumeContent();
			} catch (Exception e) {
				httpGet.abort();
				Log.e(TAG, e.toString());
				return null;
			}
			return imageFile.getAbsolutePath();
		}else {
			return null;
		}
	}

	static class getJson extends AsyncTask<String, Void, JSONArray> {

		@Override
		protected JSONArray doInBackground(String... params) {
			String Url = params[0];
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(Url);
			HttpResponse response;
			try {
				response = client.execute(httpGet);
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					HttpEntity httpEntity = response.getEntity();
					if (httpEntity != null) {
						String json = convertISToString(httpEntity.getContent())
								.trim();
						if (!json.startsWith("[")) {
							json = "[" + json + "]";
						}
						mJSONArray = new JSONArray(json);
					}
				}
			} catch (ClientProtocolException e) {
				Log.e(TAG, e.toString());
			} catch (IOException e) {
				Log.e(TAG, e.toString());
			} catch (IllegalStateException e) {
				Log.e(TAG, e.toString());
			} catch (JSONException e) {
				Log.e(TAG, e.toString());
			}
			return mJSONArray;
		}

		@Override
		protected void onPostExecute(JSONArray result) {

			super.onPostExecute(result);
		}
	}

	public static String getStreamUrl(String streamUrl) {
		String mStreamUrl = null;
		try {
			URL url = new URL(streamUrl);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setUseCaches(true);
			connection.getInputStream();
			mStreamUrl = connection.getURL().toString();
			connection.disconnect();
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return mStreamUrl;
	}

	public static String convertISToString(InputStream inputStream) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line + "\n");
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				Log.e(TAG, e.toString());
			}
		}
		return stringBuilder.toString();
	}

	public static boolean isInternetAvailable(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connManager.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
	}

}
